/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_list.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:25:34 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 17:59:24 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_parse	*get_list(t_parse *t)
{
	if (t->letter != 0 && t->conver == 0)
		work_without_conver(t);
	if (t->point && t->minusorzero == '0')
		t->minusorzero = 0;
	if (t->conver == 'p' && t->letter_flag < 3)
		t->letter_flag = 3;
	return (t);
}
