/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fill_before.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 15:20:26 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:10:44 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_fill_before(char *s, int *i, int *j)
{
	char	*res;
	int		k;

	while (s[*j] >= '0' && s[*j] <= '9')
		(*j)++;
	res = (char *)malloc(*j - *i + 1);
	*j = 0;
	while (s[*i] >= '0' && s[*i] <= '9')
		res[(*j)++] = s[(*i)++];
	res[*j] = '\0';
	*j = *i;
	k = ft_atoi(res);
	return (k);
}
