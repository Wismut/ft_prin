/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_digit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:19:29 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:55:24 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_digit(char *s, int **i, t_parse *t)
{
	int		j;

	j = **i;
	(*t).before = ft_fill_before(s, &(**i), &j);
	if (s[**i] != '.')
	{
		(**i)--;
		return ;
	}
	t->point = 1;
	(*t).after = ft_fill_after(s, &**i, &j);
}
