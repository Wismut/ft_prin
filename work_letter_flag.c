/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_letter_flag.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:22:08 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:00:59 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_letter_flag(char *s, int **i, t_parse *t)
{
	int		j;

	j = get_digit_letter(s, &*i);
	if (j > (*t).letter_flag)
		(*t).letter_flag = j;
}
