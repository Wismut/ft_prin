/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_string_char.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 15:23:50 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 15:24:17 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*fill_string_char(char *str, int *start, int count, char c)
{
	while (count-- > 0)
		str[(*start)++] = c;
	return (str);
}
