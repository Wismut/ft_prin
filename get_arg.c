/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_arg.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:28:20 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:55:04 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	*get_arg(char c, int i, void *arg)
{
	if (c == 'd' || c == 'i' || c == 'D')
		return ((void *)get_signed_arg(i, arg));
	if (c == 'o' || c == 'O' || c == 'X' || c == 'x' || c == 'u' ||
c == 'U' || c == 'p')
		return ((void *)get_unsigned_arg(i, arg));
	return (0);
}
