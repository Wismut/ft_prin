/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_with_conver.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:34:25 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 17:51:39 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_with_conver(t_parse *t, void *arg)
{
	if (is_sign_conv(t->conver))
		work_sign_conv(t, arg);
	else if (is_unsigned_conv(t->conver))
		work_unsigned_conv(t, arg);
	else if (is_char(t->conver))
		work_char(arg, t);
	else if (is_string_conv(t->conver))
		work_string_conv(t, arg);
}
