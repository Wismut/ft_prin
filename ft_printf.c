/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:36:16 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:50:09 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_printf(const char *p, ...)
{
	int		i;
	va_list	ap;
	t_parse	*t;
	int		count_symbol;

	i = -1;
	count_symbol = 0;
	va_start(ap, p);
	while (p[++i])
	{
		if (p[i] == '%' && p[i + 1] != '\0')
		{
			t = work_list((char *)p, &i);
			t->conver != 0 ? work_with_conver(t, va_arg(ap, void*)) : free_t(t);
			count_symbol += t->count;
		}
		else
		{
			count_symbol++;
			ft_charput(p[i]);
		}
	}
	va_end(ap);
	return (count_symbol);
}
