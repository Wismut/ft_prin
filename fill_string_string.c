/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_string_string.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 15:24:37 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 15:25:14 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*fill_string_string(char *src, int start, char *dst)
{
	int		i;

	i = 0;
	while (dst[i])
		src[(start)++] = dst[i++];
	return (src);
}
