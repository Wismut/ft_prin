/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 15:22:29 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:43:13 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_itoa(long long n)
{
	int					i;
	unsigned long long	m;
	char				*str;

	i = ft_sign_size(n);
	m = n > 0 ? n : -n;
	if (n < 0)
		i--;
	str = (char *)malloc(i + 1);
	str[i--] = '\0';
	while (i >= 0)
	{
		str[i--] = m % 10 + 48;
		m /= 10;
	}
	str[i] = '\0';
	return (str);
}
