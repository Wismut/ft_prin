/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_long_long.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 15:21:21 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 19:02:53 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	f_long_long(unsigned long long value, char *s, int *i, char c)
{
	const char			*str;
	unsigned long long	base;

	base = 10;
	if (c == 'x' || c == 'X' || c == 'p')
		base = 16;
	if (c == 'o')
		base = 8;
	str = c == 'X' ? "0123456789ABCDEF" : "0123456789abcdef";
	if (value >= base)
		f_long_long(value / base, s, i, c);
	s[(*i)++] = str[value % base];
}
