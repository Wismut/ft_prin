/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_zero_x.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:13:22 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:13:54 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_add_zero_x(char *s, char c)
{
	char	*str;
	int		i;

	i = 0;
	str = (char *)malloc(ft_strlen(s) + 3);
	if (s[0] == '0' && s[1] == '\0')
		str = add_zero_x_4(c, str);
	else if (s[0] == ' ' && s[1] != ' ')
		str = add_zero_x_5(s, c, str);
	else if ((s[0] >= '1' && s[0] <= '9') || (s[0] >= 'a' && s[0] <= 'f'))
		str = add_zero_x_2(s, c, str);
	else if (s[0] == '0' && s[1] == '0')
		str = add_zero_x_1(s, c, str, i);
	else if (s[0] == ' ' && s[1] == ' ')
		return (add_zero_x_3(s, c, i));
	else if (s[0] == '0' && s[1] == ' ' && s[2])
		return (add_zero_x_6(s, c));
	else if (s[0] == '\0')
	{
		str[0] = '0';
		str[1] = c == 'X' ? 'X' : 'x';
	}
	else
		return (s);
	return (str);
}
