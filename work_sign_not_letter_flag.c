/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_sign_not_letteg_flag.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:00:07 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 17:54:04 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_sign_not_letter_flag(t_parse *t, long long ll)
{
	char	*str;

	str = fill_string_sign(t, ll);
	ft_putstr(str, &t);
}
