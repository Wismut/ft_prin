/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_zero.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:14:23 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:53:11 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_add_zero(char *s)
{
	char	*str;
	int		i;

	i = 0;
	str = (char *)malloc(ft_strlen(s) + 2);
	if (s[0] == '0')
		return (s);
	else if (s[0] >= '1' && s[0] <= '9')
	{
		str[0] = '0';
		fill_string_string(str, 1, s);
	}
	else if (s[0] == ' ')
	{
		while (s[i] == ' ')
			i++;
		s[i - 1] = '0';
		return (s);
	}
	else if (s[0] == '\0')
	{
		str[0] = '0';
		str[1] = '\0';
	}
	return (str);
}
