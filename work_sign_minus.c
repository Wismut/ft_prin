/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_sign_minus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:56:52 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 17:55:20 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*work_sign_minus(const t_parse *t, long long int ll, char *s, char *str)
{
	int		i;
	int		j;
	int		k;

	k = 0;
	if (ll < 0)
		s[k++] = '-';
	else if (t->plusorspace == '+')
		s[k++] = '+';
	j = ft_sign_size(ll) - ((ll < 0 || t->plusorspace == '+') ? 1 : 0);
	i = t->after - j - ((t->plusorspace == '+' && ll >= 0) ? 1 : 0);
	while (i-- > 0)
		s[k++] = '0';
	i = ft_strlen(str);
	j = 0;
	while (i-- > 0)
		s[k++] = str[j++];
	j = ft_sign_size(ll);
	i = t->before - (get_max(j + ((t->plusorspace == '+' && ll >= 0) ? 1 : 0),
t->after + ((ll < 0 || t->plusorspace == '+') ? 1 : 0)));
	while (i-- > 0)
		s[k++] = ' ';
	s[k] = '\0';
	return (s);
}
