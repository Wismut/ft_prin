/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_unsigned_conv.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:21:53 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 17:51:13 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_unsigned_conv(t_parse *t, void *arg)
{
	char	*str;

	str = ft_itoa_base_long_long((unsigned long long)get_arg(t->conver,
t->letter_flag, arg), t->conver);
	if (t->after == 0 & t->point == 1 && str[0] == '0' && str[1] == '\0')
		str[0] = '\0';
	work_unsign_not_letter_flag(t, str);
}
