/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_string_minus.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:28:21 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 17:52:16 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*fill_string_minus(t_parse *t, char *s, char *tmp)
{
	int		i;
	int		j;
	int		k;

	j = 0;
	k = 0;
	if (t->point == 1)
		i = get_min(ft_strlen(tmp), t->after);
	else
		i = ft_strlen(tmp);
	while (j < i)
		s[k++] = tmp[j++];
	if (t->point == 1)
		i = t->before - get_min(ft_strlen(tmp), t->after);
	else
		i = t->before - ft_strlen(tmp);
	while (i-- > 0)
		s[k++] = ' ';
	s[k] = '\0';
	return (s);
}
