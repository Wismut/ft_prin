/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_signed_arg.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:27:45 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:11:34 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

long long	get_signed_arg(int i, void *arg)
{
	if (i == 0)
		return ((int)(intmax_t)arg);
	else if (i == 1)
		return ((char)(intmax_t)arg);
	else if (i == 2)
		return ((short)(intmax_t)arg);
	else if (i == 3)
		return ((long)arg);
	else if (i == 4)
		return ((long long)arg);
	else if (i == 5)
		return ((intmax_t)arg);
	else
		return ((size_t)arg);
}
