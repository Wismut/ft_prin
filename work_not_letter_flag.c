/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_not_letter_flag.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:23:02 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:01:12 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_not_letter_flag(char *s, int **i, t_parse *t)
{
	if (s[**i] == '-')
		t->minusorzero = '-';
	else if (s[**i] == '+')
		t->plusorspace = '+';
	else if (s[**i] == ' ' && t->plusorspace != '+')
		t->plusorspace = ' ';
	else if (s[**i] >= '1' && s[**i] <= '9')
		work_digit(s, &*i, t);
	else if (s[**i] == '0' && t->minusorzero != '-')
		t->minusorzero = '0';
	else if (s[**i] == '.')
		work_point(s, &*i, t);
	else if (s[**i] == '#')
		t->hash = 1;
}
