/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:26:13 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:49:05 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_parse	*work_list(char *s, int *i)
{
	t_parse	*t;

	t = initialize_list();
	while (s[++(*i)])
		if (is_not_letter_flag(s[*i]))
			work_not_letter_flag(s, &i, t);
		else if (is_letter_flag(s[*i]))
			work_letter_flag(s, &i, t);
		else if (is_conversion(s[*i]))
		{
			work_big_conv(s, &i, t);
			break ;
		}
		else if (s[*i] == '%')
		{
			t->letter == 0 ? t->letter = s[*i] : 0;
			break ;
		}
		else
		{
			t->letter = s[*i];
			if (s[*i] >= 32 && s[*i] <= 126)
				break ;
		}
	return (get_list(t));
}
