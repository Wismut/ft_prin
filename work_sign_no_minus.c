/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_sign_no_minus.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:53:44 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 17:54:19 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*work_sign_no_minus(t_parse *t, long long int ll, char *s, char *str)
{
	int		j;
	int		i;
	int		k;

	k = 0;
	j = ft_sign_size(ll);
	i = t->before - (get_max(j + ((t->plusorspace == '+' && ll >= 0) ? 1 : 0),
t->after + ((ll < 0 || t->plusorspace == '+') ? 1 : 0)));
	s = fill_string_char(s, &k, i, ' ');
	ll < 0 ? s[k++] = '-' : 0;
	(ll >= 0 && t->plusorspace == '+') ? s[k++] = '+' : 0;
	j = ft_sign_size(ll) - ((ll < 0 || t->plusorspace == '+') ? 1 : 0);
	if (ll >= 0 && t->plusorspace == ' ' && t->after >= t->before)
	{
		t->after <= t->before ? j-- : 0;
		s[k++] = ' ';
	}
	i = t->after - j - ((t->plusorspace == '+' && ll >= 0) ? 1 : 0);
	s = fill_string_char(s, &k, i, '0');
	i = ft_strlen(str);
	j = 0;
	while (i-- > 0)
		s[k++] = str[j++];
	s[k] = '\0';
	return (s);
}
