/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_zero_x_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 15:27:31 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 15:28:15 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*add_zero_x_2(char *s, char c, char *str)
{
	str[0] = '0';
	str[1] = c == 'X' ? 'X' : 'x';
	fill_string_string(str, 2, s);
	if (str[ft_strlen(str) - 1] == ' ')
		str[ft_strlen(str) - 1] = '\0';
	return (str);
}
