/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_unsigned_arg.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:27:03 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:48:39 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

unsigned long long	get_unsigned_arg(int i, void *arg)
{
	if (i == 0)
		return ((unsigned int)(uintmax_t)arg);
	else if (i == 1)
		return ((unsigned char)(uintmax_t)arg);
	else if (i == 2)
		return ((unsigned short)(uintmax_t)arg);
	else if (i == 3)
		return ((unsigned long)(uintmax_t)arg);
	else if (i == 4)
		return ((unsigned long long)arg);
	else if (i == 5)
		return ((uintmax_t)arg);
	else
		return ((size_t)arg);
}
