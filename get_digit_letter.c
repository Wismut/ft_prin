/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_digit_letter.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:21:09 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 16:21:48 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		get_digit_letter(char *s, int **i)
{
	if (s[**i] == 'l')
		if (s[**i + 1] == 'l')
		{
			(**i)++;
			return (4);
		}
		else
			return (3);
	else if (s[**i] == 'h')
		if (s[**i + 1] == 'h')
		{
			(**i)++;
			return (1);
		}
		else
			return (2);
	else if (s[**i] == 'j')
		return (5);
	else
		return (6);
}
