/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_without_conver.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:24:42 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 17:59:50 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_without_conver(t_parse *t)
{
	char	c;
	int		i;

	i = t->before;
	c = t->minusorzero == '0' ? '0' : ' ';
	if (t->before && t->minusorzero != '-')
		while (--i > 0)
			ft_putchar(c, &t);
	ft_putchar(t->letter, &t);
	i = t->before;
	if (t->before && t->minusorzero == '-')
		while (--i > 0)
			ft_putchar(' ', &t);
}
