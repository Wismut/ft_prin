/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_string_unsign.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:15:56 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:55:50 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*fill_string_unsign(t_parse *t, char *str, int str_len)
{
	int		j;
	char	*s;

	s = (char *)malloc(str_len + 1);
	s[str_len] = '\0';
	j = t->before - get_max(ft_strlen(str), t->after);
	if (t->minusorzero != '-')
		s = fill_unsign_without_minus(t, str, j, s);
	else
		s = fill_unsign_string_with_minus(t, str, s);
	if ((t->hash && ((t->conver == 'x' || t->conver == 'X') && s[0] != '0'))
		|| t->conver == 'p')
		s = ft_add_zero_x(s, t->conver);
	else if (t->hash && t->conver == 'o')
		s = ft_add_zero(s);
	return (s);
}
