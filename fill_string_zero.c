/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_string_zero.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:58:02 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 19:02:05 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*fill_string_zero(t_parse *t, long long int ll, char *s, char *str)
{
	int		j;
	int		k;

	k = 0;
	j = t->before - (ft_sign_size(ll) +
					((t->plusorspace == '+' && ll >= 0) ? 1 : 0));
	if (ll < 0)
		s[k++] = '-';
	else if (t->plusorspace == '+')
		s[k++] = '+';
	if (ll >= 0 && t->plusorspace == ' ' && t->point == 0)
	{
		j--;
		s[k++] = ' ';
	}
	while (j-- > 0)
		s[k++] = '0';
	j = 0;
	while (str[j])
		s[k++] = str[j++];
	s[k] = '\0';
	return (s);
}
