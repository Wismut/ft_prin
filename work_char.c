/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_char.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:32:11 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:53:25 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_char(void *arg, t_parse *t)
{
	char	c;
	int		i;

	i = t->before;
	c = t->minusorzero == '0' ? '0' : ' ';
	if (t->before && t->minusorzero != '-')
		while (--i > 0)
			ft_putchar(c, &t);
	c = (char)(intmax_t)arg;
	ft_putchar(c, &t);
	i = t->before;
	if (t->before && t->minusorzero == '-')
		while (--i > 0)
			ft_putchar(' ', &t);
}
