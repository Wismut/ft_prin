/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 19:30:30 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 19:30:30 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <stdio.h>
# include <stdint.h>
# include <stdarg.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct		s_parse
{
	char			conver;
	int				letter_flag;
	int				hash;
	char			minusorzero;
	char			plusorspace;
	int				point;
	int				before;
	int				after;
	int				count;
	char			letter;
}					t_parse;

int					ft_printf(const char *p, ...);
int					get_min(int a, int b);
int					ft_strlen(char *s);
void				ft_putchar(char c, t_parse **t);
void				ft_putstr(char *s, t_parse **t);
int					ft_atoi(char *s);
int					ft_sign_size(long long i);
int					ft_fill_after(char *s, int *i, int *j);
int					ft_fill_before(char *s, int *i, int *j);
void				f_long_long(unsigned long long value, char *s, int *i,
char c);
char				*ft_itoa(long long n);
char				*fill_string_char(char *str, int *start, int count,
char c);
char				*fill_string_string(char *src, int start, char *dst);
char				*add_zero_x_1(char *s, char c, char *str, int i);
char				*add_zero_x_2(char *s, char c, char *str);
char				*add_zero_x_3(char *s, char c, int i);
char				*add_zero_x_4(char c, char *str);
char				*add_zero_x_5(char *s, char c, char *str);
char				*add_zero_x_6(char *s, char c);
char				*ft_add_zero_x(char *s, char c);
char				*ft_add_zero(char *s);
char				*ft_itoa_base_long_long(unsigned long long value, char c);
int					is_conversion(char c);
int					is_not_letter_flag(char c);
int					is_letter_flag(char c);
t_parse				*initialize_list(void);
void				work_digit(char *s, int **i, t_parse *t);
void				work_point(char *s, int **i, t_parse *t);
int					get_digit_letter(char *s, int **i);
void				work_letter_flag(char *s, int **i, t_parse *t);
void				work_not_letter_flag(char *s, int **i, t_parse *t);
void				work_big_conv(char *s, int **i, t_parse *t);
void				work_without_conver(t_parse *t);
t_parse				*get_list(t_parse *t);
t_parse				*work_list(char *s, int *i);
unsigned long long	get_unsigned_arg(int i, void *arg);
long long			get_signed_arg(int i, void *arg);
void				*get_arg(char c, int i, void *arg);
void				free_t(t_parse *t);
int					is_sign_conv(char c);
int					is_unsigned_conv(char c);
int					is_string_conv(char c);
int					get_max(int a, int b);
int					max(int a, int b, int c);
char				*work_sign_no_minus(t_parse *t, long long int ll, char *s,
char *str);
char				*work_sign_minus(const t_parse *t, long long int ll,
char *s, char *str);
char				*fill_string_zero(t_parse *t, long long int ll, char *s,
char *str);
char				*fill_string_sign(t_parse *t, long long ll);
void				work_sign_not_letter_flag(t_parse *t, long long ll);
void				work_sign_conv(t_parse *t, void *arg);
char				*fill_unsign_without_minus(t_parse *t, char *str, int j,
char *s);
char				*fill_unsign_string_with_minus(t_parse *t, char *str,
char *s);
char				*fill_string_unsign(t_parse *t, char *str, int str_len);
void				work_unsign_not_letter_flag(t_parse *t, char *str);
void				work_unsigned_conv(t_parse *t, void *arg);
char				*fill_string_without_minus(const t_parse *t, char *s,
char *tmp, int i);
char				*fill_string_minus(t_parse *t, char *s, char *tmp);
void				work_string_conv(t_parse *t, void *arg);
int					is_char(char c);
void				work_char(void *arg, t_parse *t);
void				work_with_conver(t_parse *t, void *arg);
void				ft_charput(char c);

#endif
