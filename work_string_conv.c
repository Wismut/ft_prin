/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_string_conv.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:31:04 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:13:05 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_string_conv(t_parse *t, void *arg)
{
	char	*s;
	char	*tmp;
	int		i;

	if (arg == NULL)
	{
		tmp = NULL;
		ft_putstr(tmp, &t);
		return ;
	}
	tmp = (char *)arg;
	s = (char *)malloc(max(t->before, t->after, ft_strlen(tmp)) + 1);
	if (t->point == 1)
		i = t->before - get_min(ft_strlen(tmp), t->after);
	else
		i = t->before - ft_strlen(tmp);
	if (t->minusorzero != '-')
		s = fill_string_without_minus(t, s, tmp, i);
	else
		s = fill_string_minus(t, s, tmp);
	ft_putstr(s, &t);
}
