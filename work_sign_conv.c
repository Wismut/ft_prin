/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_sign_conv.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:01:00 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:52:29 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_sign_conv(t_parse *t, void *arg)
{
	long long	ll;

	ll = (long long)get_arg(t->conver, t->letter_flag, arg);
	if (t->before == 0 && t->after == 0 & t->point == 1 && ll == 0)
		return ;
	work_sign_not_letter_flag(t, ll);
}
