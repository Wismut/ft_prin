/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_string_without_minus.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:25:48 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 19:01:19 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*fill_string_without_minus(const t_parse *t, char *s, char *tmp, int i)
{
	int		k;
	int		j;

	j = 0;
	k = 0;
	while (i-- > 0)
		s[k++] = t->minusorzero == '0' ? '0' : ' ';
	if (t->point)
		i = get_min(ft_strlen(tmp), t->after);
	else
		i = ft_strlen(tmp);
	while (j < i)
		s[k++] = tmp[j++];
	s[k] = '\0';
	return (s);
}
