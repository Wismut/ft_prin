/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_string_sign.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:58:49 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:52:04 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*fill_string_sign(t_parse *t, long long ll)
{
	int		i;
	char	*s;
	char	*str;

	i = max(t->after, t->before, ft_sign_size(ll)) + 1 +
((ll < 0 || t->plusorspace == '+') ? 1 : 0);
	s = (char *)malloc(i);
	str = ft_itoa(ll);
	if (str[0] == '0' && t->after == 0 && t->point)
		str[0] = ' ';
	if (t->minusorzero == '0' && t->after == 0)
		s = fill_string_zero(t, ll, s, str);
	else
	{
		if (t->minusorzero != '-')
			s = work_sign_no_minus(t, ll, s, str);
		else
			s = work_sign_minus(t, ll, s, str);
	}
	return (s);
}
