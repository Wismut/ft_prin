/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_unsign_string_with_minus.c                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:03:58 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:49:45 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*fill_unsign_string_with_minus(t_parse *t, char *str, char *s)
{
	int		j;
	int		k;

	k = 0;
	j = ft_strlen(str);
	while (t->after > j++)
		s[k++] = '0';
	j = 0;
	while (str[j])
		s[k++] = str[j++];
	j = t->before - get_max(ft_strlen(str), t->after);
	s = fill_string_char(s, &k, j, ' ');
	s[k] = '\0';
	return (s);
}
