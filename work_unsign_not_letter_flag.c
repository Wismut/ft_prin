/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   work_unsign_not_letter_flag.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:20:57 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 17:53:48 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	work_unsign_not_letter_flag(t_parse *t, char *str)
{
	int		str_len;

	str_len = max(t->after, t->before, ft_strlen(str));
	if (t->plusorspace == '+')
		str_len++;
	str = fill_string_unsign(t, str, str_len);
	ft_putstr(str, &t);
}
