/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_list.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:18:43 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:00:14 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_parse	*initialize_list(void)
{
	t_parse	*t;

	t = (t_parse *)malloc(sizeof(t_parse));
	t->before = 0;
	t->after = 0;
	t->conver = 0;
	t->hash = 0;
	t->letter_flag = 0;
	t->minusorzero = 0;
	t->plusorspace = 0;
	t->point = 0;
	t->count = 0;
	t->letter = 0;
	return (t);
}
