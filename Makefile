#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mivanov <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/24 16:55:45 by mivanov           #+#    #+#              #
#    Updated: 2017/02/13 17:42:08 by mivanov          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME =  libftprintf.a

CFLAGS = -Wall -Wextra -Werror

INCLUDES = -I .

OBJECTS = $(SRC:.c=.o)

SRC = ft_printf.c \
		get_min.c \
		ft_strlen.c \
		ft_putchar.c \
		ft_putstr.c \
		ft_atoi.c \
		ft_sign_size.c \
		ft_fill_after.c \
		ft_fill_before.c \
		f_long_long.c \
		ft_itoa.c \
		fill_string_char.c \
		fill_string_string.c \
		add_zero_x_1.c \
		add_zero_x_2.c \
		add_zero_x_3.c \
		add_zero_x_4.c \
		add_zero_x_5.c \
		add_zero_x_6.c \
		ft_add_zero_x.c \
		ft_add_zero.c \
		ft_itoa_base_long_long.c \
		is_conversion.c \
		is_not_letter_flag.c \
		is_letter_flag.c \
		initialize_list.c \
		work_digit.c \
		work_point.c \
		get_digit_letter.c \
		work_letter_flag.c \
		work_not_letter_flag.c \
		work_big_conv.c \
		work_without_conver.c \
		get_list.c \
		work_list.c \
		get_unsigned_arg.c \
		get_signed_arg.c \
		get_arg.c \
		free_t.c \
		is_sign_conv.c \
		is_unsigned_conv.c \
		is_string_conv.c \
		get_max.c \
		max.c \
		work_sign_no_minus.c \
		work_sign_minus.c \
		fill_string_zero.c \
		fill_string_sign.c \
		work_sign_not_letter_flag.c \
		work_sign_conv.c \
		fill_unsign_without_minus.c \
		fill_unsign_string_with_minus.c \
		fill_string_unsign.c \
		work_unsign_not_letter_flag.c \
		work_unsigned_conv.c \
		fill_string_without_minus.c \
		fill_string_minus.c \
		work_string_conv.c \
		is_char.c \
		work_char.c \
		work_with_conver.c \
		ft_charput.c


all : $(NAME)

$(NAME) :
		gcc -c $(CFLAGS) $(SRC) $(INCLUDES)
		ar rc $(NAME) $(OBJECTS)

clean :
		rm -rf $(OBJECTS)

fclean : clean
		rm -rf $(NAME)

re : fclean all
