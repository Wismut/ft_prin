/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_unsign_without_minus.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:01:52 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 17:56:15 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*fill_unsign_without_minus(t_parse *t, char *str, int j, char *s)
{
	int		k;

	k = 0;
	s = fill_string_char(s, &k, j, (t->minusorzero == '0' && t->before > 0 ?
'0' : ' '));
	j = ft_strlen(str);
	while (t->after > j++)
		s[k++] = '0';
	j = 0;
	while (str[j])
		s[k++] = str[j++];
	s[k] = '\0';
	return (s);
}
