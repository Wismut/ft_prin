/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base_long_long.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:15:20 by mivanov           #+#    #+#             */
/*   Updated: 2017/02/13 18:44:54 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_itoa_base_long_long(unsigned long long value, char c)
{
	char				*s;
	int					i;
	unsigned long long	base;

	i = 0;
	base = 10;
	if (c == 'x' || c == 'X' || c == 'p')
		base = 16;
	if (c == 'o')
		base = 8;
	if (base < 2 || base > 16 || !(s = (char *)malloc(32)))
		return (NULL);
	f_long_long(value, s, &i, c);
	s[i] = '\0';
	return (s);
}
